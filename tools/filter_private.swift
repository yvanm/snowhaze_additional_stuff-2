import Foundation

let parallelism = 7

var count = 0

var results = [[String]?](count: parallelism, repeatedValue: nil)

func getDomains(fromFile file: String) -> [String] {
	let data = try! String(contentsOfFile: file)
	let lines = data.componentsSeparatedByString("\n")
	return lines.filter {!$0.isEmpty}
}

let rx = try! NSRegularExpression(pattern: "^www\\.", options: NSRegularExpressionOptions())

let popularSites = getDomains(fromFile: "PopularSites.domains")
let privateSites = getDomains(fromFile: "PrivateSites.domains")

for id in 0 ..< parallelism {
	let startIndex = id * popularSites.count / parallelism
	let endIndex = (id + 1) * popularSites.count / parallelism
	let localDomains = popularSites[startIndex ..< endIndex]
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
		var res = [String]()
		res.reserveCapacity(localDomains.count)
		for domain in localDomains {
			if privateSites.contains(domain) {
				continue
			}
			if privateSites.contains("www." + domain) {
				continue
			}
			if domain.hasPrefix("www.") {
				let start = domain.startIndex.advancedBy(4)
				let noWWW = domain.substringFromIndex(start)
				if privateSites.contains(noWWW) {
					continue
				}
			}
			res.append(domain)
			count += 1
		}
		results[id] = res
	}
}

var oldPercent = 0
wait: while true {
	sleep(1)
	let percent = 100 * count / popularSites.count
	if percent > oldPercent {
		oldPercent = percent
		print("\(percent)%")
	}
	for result in results {
		if result == nil {
			continue wait
		}
	}
	break
}
for result in results {
	for domain in result! {
		print(domain)
	}
}